import django_filters.rest_framework
from rest_framework import filters
from django.shortcuts import render
from rest_framework import generics
from .models import BooksAuthor, BooksBook
from .serializers import AuthorSerializer, BookSerializer
# Create your views here.

class AuthorList(generics.ListCreateAPIView):
    queryset = BooksAuthor.objects.all()
    serializer_class = AuthorSerializer
    filter_backends = [filters.SearchFilter]
    search_fields =  ['name']

class BookList(generics.ListCreateAPIView):
    queryset = BooksBook.objects.all()
    serializer_class = BookSerializer
    filter_backends = [filters.SearchFilter, django_filters.rest_framework.DjangoFilterBackend]
    filterset_fields = ['gutenberg_id']
    search_fields = ['title']