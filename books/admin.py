from django.contrib import admin
from . import models
# Register your models here.

admin.site.register(models.BooksAuthor)
admin.site.register(models.BooksBook)
admin.site.register(models.BooksBookAuthors)
admin.site.register(models.BooksBookBookshelves)
admin.site.register(models.BooksBookLanguages)
admin.site.register(models.BooksBookSubjects)
admin.site.register(models.BooksBookshelf)
admin.site.register(models.BooksLanguage)
admin.site.register(models.BooksSubject)

