from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from books import views

from rest_framework.schemas import get_schema_view

urlpatterns = [
    path('openapi/', get_schema_view(
        title="Your Project",
        description="API for all things",
        version="1.0.0"
    ), name='openapi-schema'),
    path('authors/', views.AuthorList.as_view()),
    path('books/', views.BookList.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)