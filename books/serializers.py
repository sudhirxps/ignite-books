from rest_framework import serializers
from .models import BooksAuthor, BooksBook

class AuthorSerializer(serializers.ModelSerializer):

    class Meta:
        model = BooksAuthor
        fields = ('name',)


    
class BookSerializer(serializers.ModelSerializer):

    class Meta:
        model = BooksBook
        fields = ('title', 'gutenberg_id')
